import { Link } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import React, { useState } from 'react';
import { MdDarkMode } from 'react-icons/md';
import { SiThemoviedatabase } from "react-icons/si";
import { BiCameraMovie, BiMoviePlay } from "react-icons/bi";

function Navbar(props) {
  const [searchStr, setSearchStr] = useState('')
  const handleSearch = (e) => {
    setSearchStr(e.target.value);
  }
  const redirectTomovies = () => {
    window.location.href = "search?name=" + searchStr;
  }

  return (
    <> <div className='container-fluid text-light' style={{ backgroundColor: "#1e272e" }}>
      <div className="container  text-light">
        <header className="p-3 mb-3 border-bottom">
          <div className="container">
            <div className="d-flex flex-wrap align-items-start justify-content-between">
              <div className="d-flex link-light text-light">
                {/* <Link
                  to=""
                  className="nav-link px-2 justify-content-start link-light"
                > 
                  {' '}
                  <BiMoviePlay/>
                  {' '}
                  Home
                </Link> */}
                <Link to="popular" className="nav-link px-2 link-light">
                  {' '}
                  Popular
                </Link>
                <Link to="toprated" className="nav-link px-2 link-light">
                  Top Rated
                </Link>
                <Link to="upcoming" className="nav-link px-2 link-light">
                  Upcoming
                </Link>

                <input type="text" onChange={(e) => handleSearch(e)} placeholder="Search.." />
                <button onClick={() => redirectTomovies()}>Search</button>
                {/* <form class="form-inline my-2 my-lg-0">
      <input className="form-control mr-sm-2" type="search"  onChange={(e) => handleSearch(e)}  placeholder="Movie name" aria-label="Search"/> 
      <button style={{color:"white", backgroundColor:"grey"}} onClick={() => redirectTomovies()} className="btn my-2 my-sm-0" type="submit">Search</button>
    </form> */}

              </div>
            </div>
          </div>
        </header>
      </div>
    </div>
    </>
  );
}

export default Navbar;
